package com.ponykamni.photogallery

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.annotation.WorkerThread
import com.ponykamni.photogallery.api.FlickrApi
import okhttp3.ResponseBody
import retrofit2.Response

class FlickrFetchr {

    private val flickrApi: FlickrApi = FlickrApi.getApi()

    @WorkerThread
    fun fetchPhoto(url: String): Bitmap? {
        val response: Response<ResponseBody> = flickrApi.fetchUrlBytes(url).execute()
        val bitmap = response.body()?.byteStream()?.use(BitmapFactory::decodeStream)
        Log.i(TAG, "Decoded bitmap=$bitmap from Response=$response")
        return bitmap
    }

    companion object {

        private const val TAG = "FlickrFetchr"
    }
}