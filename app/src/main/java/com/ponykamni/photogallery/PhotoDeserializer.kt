package com.ponykamni.photogallery

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.ponykamni.photogallery.api.PhotoResponse
import java.lang.reflect.Type

class PhotoDeserializer : JsonDeserializer<PhotoResponse> {
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): PhotoResponse {

        val photos = json.asJsonObject.get(PHOTOS_KEY)
        return Gson().fromJson<PhotoResponse>(photos.asJsonObject, PhotoResponse::class.java)
    }

    companion object {

        private const val PHOTOS_KEY = "photos"
    }
}