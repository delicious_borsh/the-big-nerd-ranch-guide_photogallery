package com.ponykamni.photogallery

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.ponykamni.photogallery.PhotoGalleryApplication.Companion.NOTIFICATION_CHANNEL_ID
import com.ponykamni.photogallery.api.FlickrApi
import com.ponykamni.photogallery.model.GalleryItem
import com.ponykamni.photogallery.ui.PhotoGalleryActivity

class PollWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {
        Log.i(TAG, "Work request triggered")
        val query = QueryPreferences.getStoredQuery(context)
        val lastResultId = QueryPreferences.getLastResultId(context)
        val items: List<GalleryItem> = if (query.isEmpty()) {
            FlickrApi.getApi().fetchPhotos(1)
                .execute()
                .body()
                ?.galleryItems
        } else {
            FlickrApi.getApi().searchPhotos(query, 1)
                .execute()
                .body()
                ?.galleryItems
        } ?: emptyList()

        if (items.isEmpty()) {
            return Result.success()
        }

        val resultId = items.first().id
        if (resultId == lastResultId) {
            Log.i(TAG, "Got an old result: $resultId for query $query")
        } else {
            Log.i(TAG, "Got a new result: $resultId for query $query")
            QueryPreferences.setLastResultId(context, resultId)

            showNewPhotosNotification()

        }

        return Result.success()
    }

    private fun showNewPhotosNotification() {
        val intent = PhotoGalleryActivity.newIntent(context)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val resources = context.resources
        val notification = NotificationCompat
            .Builder(context, NOTIFICATION_CHANNEL_ID)
            .setTicker(resources.getString(R.string.new_pictures_title))
            .setSmallIcon(android.R.drawable.ic_menu_report_image)
            .setContentTitle(resources.getString(R.string.new_pictures_title))
            .setContentText(resources.getString(R.string.new_pictures_text))
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        showBackgroundNotification(0, notification)
    }

    private fun showBackgroundNotification(
        requestCode: Int,
        notification: Notification
    ) {
        val intent = Intent(ACTION_SHOW_NOTIFICATION).apply {
            putExtra(REQUEST_CODE, requestCode)
            putExtra(NOTIFICATION, notification)
        }
        context.sendOrderedBroadcast(intent, PERM_PRIVATE)
    }

    companion object {
        const val ACTION_SHOW_NOTIFICATION = "com.ponykamni.photogallery.SHOW_NOTIFICATION"
        const val PERM_PRIVATE = "com.ponykamni.photogallery.PRIVATE"
        const val REQUEST_CODE = "REQUEST_CODE"
        const val NOTIFICATION = "NOTIFICATION"
        private const val TAG = "PollWorker"
    }
}