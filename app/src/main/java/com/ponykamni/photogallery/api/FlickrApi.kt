package com.ponykamni.photogallery.api

import com.google.gson.GsonBuilder
import com.ponykamni.photogallery.PhotoDeserializer
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface FlickrApi {

    @GET("services/rest?method=flickr.interestingness.getList")
    fun fetchPhotos(@Query("page") page: Int): Call<PhotoResponse>

    @GET
    fun fetchUrlBytes(@Url url: String): Call<ResponseBody>

    @GET("services/rest?method=flickr.photos.search")
    fun searchPhotos(@Query("text") query: String, @Query("page") page: Int): Call<PhotoResponse>

    companion object {

        fun getApi(): FlickrApi {
            val gson = GsonBuilder()
                .registerTypeAdapter(PhotoResponse::class.java, PhotoDeserializer())
                .create()

            val client = OkHttpClient.Builder()
                .addInterceptor(PhotoInterceptor())
                .build()

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl("https://api.flickr.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
            return retrofit.create(FlickrApi::class.java)
        }
    }
}