package com.ponykamni.photogallery.api

import com.google.gson.annotations.SerializedName
import com.ponykamni.photogallery.model.GalleryItem

class PhotoResponse {
    @SerializedName("photo")
    lateinit var galleryItems: List<GalleryItem>
    var page: Int = 0
    var pages: Int = 0

    fun hasMorePages(): Boolean = page < pages
}