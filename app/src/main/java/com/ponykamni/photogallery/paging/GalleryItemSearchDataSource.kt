package com.ponykamni.photogallery.paging


import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.ponykamni.photogallery.api.FlickrApi
import com.ponykamni.photogallery.api.PhotoResponse
import com.ponykamni.photogallery.model.GalleryItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GalleryItemSearchDataSource(
    private val searchText: String
) : PageKeyedDataSource<Int, GalleryItem>() {

    private val flickrApi: FlickrApi = FlickrApi.getApi()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, GalleryItem>
    ) {
        flickrApi
            .searchPhotos(searchText, FIRST_PAGE)
            .enqueue(object : Callback<PhotoResponse> {

                override fun onFailure(call: Call<PhotoResponse>, t: Throwable) {
                    Log.e(TAG, "Failed to fetch photos", t)
                }

                override fun onResponse(
                    call: Call<PhotoResponse>,
                    response: Response<PhotoResponse>
                ) {
                    Log.d(TAG, "Response received")

                    val photoResponse = response.body()

                    if (photoResponse != null) {

                        callback.onResult(
                            getItemsFromResponse(photoResponse),
                            null,
                            FIRST_PAGE + 1
                        )
                    }
                }
            })
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GalleryItem>
    ) {
        flickrApi
            .searchPhotos(searchText, params.key)
            .enqueue(object : Callback<PhotoResponse> {

                override fun onFailure(call: Call<PhotoResponse>, t: Throwable) {
                    Log.e(TAG, "Failed to fetch photos", t)
                }

                override fun onResponse(
                    call: Call<PhotoResponse>,
                    response: Response<PhotoResponse>
                ) {
                    Log.d(TAG, "Response received")

                    val photoResponse = response.body()

                    if (photoResponse != null) {

                        val key = if (params.key > 1) params.key - 1 else null

                        callback.onResult(
                            getItemsFromResponse(photoResponse),
                            key
                        )
                    }
                }
            })
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, GalleryItem>
    ) {
        flickrApi
            .searchPhotos(searchText, params.key)
            .enqueue(object : Callback<PhotoResponse> {

                override fun onFailure(call: Call<PhotoResponse>, t: Throwable) {
                    Log.e(TAG, "Failed to fetch photos", t)
                }

                override fun onResponse(
                    call: Call<PhotoResponse>,
                    response: Response<PhotoResponse>
                ) {
                    Log.d(TAG, "Response received")

                    val photoResponse = response.body()

                    if (photoResponse != null) {

                        val key = if (photoResponse.hasMorePages()) params.key + 1 else null

                        callback.onResult(
                            getItemsFromResponse(photoResponse),
                            key
                        )
                    }
                }
            })
    }

    private fun getItemsFromResponse(response: PhotoResponse): List<GalleryItem> {
        return response.galleryItems.filterNot {
            it.url.isBlank()
        }
    }

    companion object {
        private const val TAG = "GalleryItemDataSource"
        private const val FIRST_PAGE = 1
    }
}