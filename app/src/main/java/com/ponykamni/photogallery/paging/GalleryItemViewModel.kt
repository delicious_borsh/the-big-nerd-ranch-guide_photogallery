package com.ponykamni.photogallery.paging

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ponykamni.photogallery.QueryPreferences
import com.ponykamni.photogallery.model.GalleryItem


class GalleryItemViewModel(private val app: Application) : AndroidViewModel(app) {

    var itemPagedList: LiveData<PagedList<GalleryItem?>>

    private val queryText = MutableLiveData<String>()

    val searchTerm: LiveData<String> = queryText

    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setPageSize(PAGE_SIZE)
        .build()

    init {
        itemPagedList = Transformations.switchMap(queryText) { input: String ->
            LivePagedListBuilder(DataSourceFactory(input), pagedListConfig)
                .build()
        }

        queryText.value = QueryPreferences.getStoredQuery(app)
    }

    fun fetchPhotos(query: String = "") {
        QueryPreferences.setStoredQuery(app, query)
        queryText.value = query
    }

    companion object {

        private const val PAGE_SIZE = 10
    }

    inner class DataSourceFactory(
        private val searchQuery: String
    ) : DataSource.Factory<Int, GalleryItem>() {

        override fun create(): DataSource<Int, GalleryItem> {

            return if (searchQuery.isBlank()) {
                GalleryItemDataSource()
            } else {
                GalleryItemSearchDataSource(searchQuery)
            }
        }
    }
}