package com.ponykamni.photogallery.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ponykamni.photogallery.R

class PhotoGalleryActivity : AppCompatActivity(),
    PhotoGalleryFragment.Callback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_gallery)

        val isFragmentContainerEmpty = savedInstanceState == null
        if (isFragmentContainerEmpty) {
            supportFragmentManager
                .beginTransaction()
                .add(
                    R.id.fragment_container,
                    PhotoGalleryFragment.newInstance()
                )
                .commit()
        }
    }

    override fun onProgress() {
        supportFragmentManager
            .beginTransaction()
            .add(
                R.id.fragment_container,
                ProgressFragment.newInstance()
            )
            .commit()
    }

    override fun onLoadFinished() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        if (currentFragment is ProgressFragment) {
            supportFragmentManager
                .beginTransaction()
                .remove(currentFragment)
                .commit()
        }
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, PhotoGalleryActivity::class.java)
        }
    }
}
