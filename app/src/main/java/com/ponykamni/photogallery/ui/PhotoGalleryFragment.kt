package com.ponykamni.photogallery.ui

import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.ponykamni.photogallery.PollWorker
import com.ponykamni.photogallery.QueryPreferences
import com.ponykamni.photogallery.R
import com.ponykamni.photogallery.model.GalleryItem
import com.ponykamni.photogallery.paging.GalleryItemViewModel
import com.ponykamni.photogallery.thumbnails.ThumbnailDownloader
import java.util.concurrent.TimeUnit


class PhotoGalleryFragment : VisibleFragment() {

    private val itemViewModel: GalleryItemViewModel by lazy {
        ViewModelProvider(this).get(GalleryItemViewModel::class.java)
    }

    private lateinit var photoRecyclerView: RecyclerView
    private lateinit var thumbnailDownloader: ThumbnailDownloader<PhotoHolder>

    private var callback: Callback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (activity !is Callback) throw IllegalStateException("Activity must implement Callback!!!")
        callback = activity as Callback

        retainInstance = true
        setHasOptionsMenu(true)

        val responseHandler = Handler()
        thumbnailDownloader =
            ThumbnailDownloader(responseHandler) { photoHolder, bitmap ->
                val drawable = BitmapDrawable(resources, bitmap)
                photoHolder.bindDrawable(drawable)
            }

        lifecycle.addObserver(thumbnailDownloader.fragmentLifecycleObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view = inflater.inflate(R.layout.fragment_photo_gallery, container, false)
        photoRecyclerView = view.findViewById(R.id.photo_recycler_view)

        photoRecyclerView.layoutManager = GridLayoutManager(context, 1)

        photoRecyclerView.viewTreeObserver.addOnGlobalLayoutListener {
            val columnsQuantity = photoRecyclerView.width / COLUMN_WIDTH
            (photoRecyclerView.layoutManager as GridLayoutManager).spanCount = columnsQuantity
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PhotoAdapter()

        itemViewModel.itemPagedList.observe(
            viewLifecycleOwner,
            Observer<PagedList<GalleryItem?>> { items -> adapter.submitList(items) })

        photoRecyclerView.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        thumbnailDownloader.clearQueue()
    }

    override fun onDestroy() {
        super.onDestroy()

        lifecycle.removeObserver(
            thumbnailDownloader.fragmentLifecycleObserver
        )
        callback = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_photo_gallery, menu)

        val searchItem: MenuItem = menu.findItem(R.id.menu_item_search)
        val searchView = searchItem.actionView as SearchView

        itemViewModel.searchTerm.observe(viewLifecycleOwner, Observer {
            searchView.setQuery(it, false)
        })

        searchView.apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(queryText: String): Boolean {
                    Log.d(TAG, "QueryTextSubmit: $queryText")
                    itemViewModel.fetchPhotos(queryText)
                    searchView.onActionViewCollapsed()
                    callback?.onProgress()
                    return true
                }

                override fun onQueryTextChange(queryText: String): Boolean {
                    Log.d(TAG, "QueryTextChange: $queryText")
                    return false
                }
            })
            setOnSearchClickListener {
                searchView.setQuery(itemViewModel.searchTerm.value ?: "", false)
            }
        }
        val toggleItem = menu.findItem(R.id.menu_item_toggle_polling)
        val isPolling =
            QueryPreferences.isPolling(requireContext())
        val toggleItemTitle = if (isPolling) {
            R.string.stop_polling
        } else {
            R.string.start_polling
        }
        toggleItem.setTitle(toggleItemTitle)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_clear -> {
                itemViewModel.fetchPhotos("")
                callback?.onProgress()
                true
            }
            R.id.menu_item_toggle_polling -> {
                managePolling()
                activity?.invalidateOptionsMenu()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun managePolling() {
        val isPolling =
            QueryPreferences.isPolling(requireContext())
        if (isPolling) {
            WorkManager.getInstance().cancelUniqueWork(POLL_WORK)
            QueryPreferences.setPolling(
                requireContext(),
                false
            )
        } else {
            val constraints = Constraints.Builder()
//                .setRequiredNetworkType(NetworkType.UNMETERED)
                .build()
            val periodicRequest = PeriodicWorkRequest
                .Builder(PollWorker::class.java, 15, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build()
            WorkManager.getInstance().enqueueUniquePeriodicWork(
                POLL_WORK,
                ExistingPeriodicWorkPolicy.KEEP,
                periodicRequest
            )
            QueryPreferences.setPolling(
                requireContext(),
                true
            )
        }
    }

    private inner class PhotoHolder(private val itemImageView: ImageView) :
        RecyclerView.ViewHolder(itemImageView), View.OnClickListener {

        private lateinit var galleryItem: GalleryItem

        init {
            itemView.setOnClickListener(this)
        }

        fun bindGalleryItem(item: GalleryItem) {
            galleryItem = item
        }

        override fun onClick(view: View) {
            val intent = PhotoPageActivity.newIntent(requireContext(), galleryItem.photoPageUri)
            startActivity(intent)
        }

        val bindDrawable: (Drawable) -> Unit = itemImageView::setImageDrawable
    }

    private inner class PhotoAdapter :
        PagedListAdapter<GalleryItem, PhotoHolder>(
            diffUtilCallback
        ) {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): PhotoHolder {
            val view = layoutInflater.inflate(
                R.layout.list_item_gallery,
                parent,
                false
            ) as ImageView
            return PhotoHolder(
                view
            )
        }

        override fun onBindViewHolder(holder: PhotoHolder, position: Int) {
            val galleryItem = getItem(position)

            if (galleryItem != null) {
                holder.bindGalleryItem(galleryItem)
                val placeholder: Drawable = ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.bill_up_close
                ) ?: ColorDrawable()
                holder.bindDrawable(placeholder)

                thumbnailDownloader.queueThumbnail(holder, galleryItem.url)
                callback?.onLoadFinished()
            }
        }

    }

    interface Callback {

        fun onProgress()
        fun onLoadFinished()
    }

    companion object {

        private val diffUtilCallback: DiffUtil.ItemCallback<GalleryItem> =
            object : DiffUtil.ItemCallback<GalleryItem>() {
                override fun areItemsTheSame(
                    oldItem: GalleryItem,
                    newItem: GalleryItem
                ): Boolean {
                    return oldItem === newItem
                }

                override fun areContentsTheSame(
                    oldItem: GalleryItem,
                    newItem: GalleryItem
                ): Boolean {
                    return oldItem == newItem
                }
            }

        private const val TAG = "PhotoGalleryFragment"
        private const val COLUMN_WIDTH = 300
        private const val POLL_WORK = "POLL_WORK"

        fun newInstance() = PhotoGalleryFragment()
    }
}